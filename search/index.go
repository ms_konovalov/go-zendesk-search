package search

// Package search contains structures and functions for working with search index.
// The search index is based on Trie data structure which is a bit less resource consuming comparing to simple hash maps.
// Only full value matching is available. All data types are currently treated as strings.
// For the sake of extensibility input data is expected in the model.Item form which is essentially a map of key-value pairs.
// List of fields to index getting passed as an external parameter to allow searching with empty field's value
// Independent index is being created for each field in the list
import (
	"zendesk-search/model"

	"github.com/dghubble/trie"
)

// Index - full index for all available collections
type Index struct {
	byCollection map[string]*byCollection
}

// byCollection - independent index for each collection
type byCollection struct {
	byField map[string]*byField
}

// byField - independent index for each indexed field
type byField = trie.RuneTrie

func CreateIndex() *Index {
	return &Index{
		byCollection: make(map[string]*byCollection),
	}
}

// Get searches index for a provided value and returns a slice of found items
func (idx *Index) Get(collection string, field string, key string) []model.Item {
	c, ok := idx.byCollection[collection]
	if !ok {
		return nil
	}
	return c.get(field, key)
}

// getOrCreateCollection lazily creates empty index for the provided collection
func (idx *Index) getOrCreateCollection(name string) *byCollection {
	c, ok := idx.byCollection[name]
	if !ok {
		newCol := &byCollection{
			byField: make(map[string]*byField),
		}
		idx.byCollection[name] = newCol
		return newCol
	}
	return c
}

// put adds the new item to the index. The value always gets converted to the slice.
// So if the value already exists in the index it will be appended to the slice.
func (idx *byCollection) put(field string, key string, value model.Item) {
	elem, ok := idx.byField[field]
	if !ok {
		elem = trie.NewRuneTrie()
		idx.byField[field] = elem
	}
	cur := elem.Get(key)
	if cur == nil {
		elem.Put(key, []model.Item{value})
		return
	}
	cur = append(cur.([]model.Item), value)
	elem.Put(key, cur)
}

func (idx *byCollection) get(field string, key string) []model.Item {
	f, ok := idx.byField[field]
	if !ok {
		return nil
	}
	res := f.Get(key)
	if res == nil {
		return nil
	}
	return f.Get(key).([]model.Item)
}
