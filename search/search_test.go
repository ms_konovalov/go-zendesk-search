package search

import (
	"testing"

	"zendesk-search/data"
	"zendesk-search/model"

	"github.com/stretchr/testify/assert"
)

func TestFullFlow(t *testing.T) {
	idx := CreateIndex()

	err := idx.IndexData(data.SupportedTypes)
	assert.NoError(t, err)
	// search by User
	v := idx.SearchWithJoin(data.UserType.Name, "name", "Loraine Pittman")
	assert.Len(t, v, 1)
	assert.Len(t, v[0]["tickets"], 4)
	// search by Ticket
	v = idx.SearchWithJoin(data.TicketType.Name, "subject", "A Catastrophe in Hungary")
	assert.Len(t, v, 1)
	assert.NotNil(t, v[0]["assignee_name"])
	// search by array field
	assert.Len(t, idx.SearchWithJoin(data.TicketType.Name, "tags", "Puerto Rico"), 14)
	// search by empty field
	assert.Len(t, idx.SearchWithJoin(data.TicketType.Name, "assignee_id", model.ToString(nil)), 4)
}
