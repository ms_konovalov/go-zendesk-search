package data

// Package data contains functions for reading data from files and in particular from embedded files `tickets.json` and `users.json`
import (
	"embed"
	"encoding/json"
	"io/ioutil"

	"zendesk-search/model"
)

//go:embed *.json
var efs embed.FS

func ReadUsers() ([]model.Item, error) {
	f, err := efs.Open("users.json")
	if err != nil {
		return nil, err
	}
	b, _ := ioutil.ReadAll(f)
	return read(b)
}

func ReadTickets() ([]model.Item, error) {
	f, err := efs.Open("tickets.json")
	if err != nil {
		return nil, err
	}
	b, _ := ioutil.ReadAll(f)
	return read(b)
}

func read(b []byte) ([]model.Item, error) {
	var res []model.Item
	err := json.Unmarshal(b, &res)
	return res, err
}

// Meta-data about Users and Tickets collections

var UserType = model.DataType{Name: "Users", Load: ReadUsers, Fields: []string{"_id", "name", "created_at", "verified"}}

var TicketType = model.DataType{Name: "Tickets", Load: ReadTickets, Fields: []string{"_id", "created_at", "type", "subject", "assignee_id", "tags"}}

var Joins = []model.Join{
	{First: model.JoinParty{Collection: &UserType, KeyField: "_id", Alias: "tickets", ForeignField: "subject"},
		Second: model.JoinParty{Collection: &TicketType, KeyField: "assignee_id", Alias: "assignee_name", ForeignField: "name"}},
}

var SupportedTypes = []model.DataType{UserType, TicketType}
