package model

import (
	"fmt"
	"reflect"
	"strings"
)

// Package model contains data structures to describe data model of the search app

type Item = map[string]interface{}

type Value = interface{}

func ToString(v Value) string {
	if IfArray(v) {
		values := v.([]interface{})
		strs := make([]string, 0)
		for _, v := range values {
			strs = append(strs, ToString(v))
		}
		return "'" + strings.Join(strs, "', '") + `'`
	}
	return fmt.Sprintf("%v", v)
}

func IfArray(v Value) bool {
	rt := reflect.TypeOf(v)
	return rt != nil && (rt.Kind() == reflect.Slice || rt.Kind() == reflect.Array)
}

// DataType struct represents meta-data about collection.
type DataType struct {
	Name   string
	Load   func() ([]Item, error)
	Fields []string
}

// Join struct represents relations between 2 collections First and Second.
type Join struct {
	First  JoinParty
	Second JoinParty
}

// JoinParty struct contains meta-data about one party of the Join relations
type JoinParty struct {
	Collection *DataType
	// Name of the field that contains foreign key
	KeyField string
	// Alias for the new field in which the joined data should appear
	Alias string
	// Field name in the foreign collection
	ForeignField string
}
