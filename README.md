# Zendesk search

## OVERVIEW

Using the provided data (tickets.json and users.json) write a simple command line application to search the data and return the results in a human-readable format.
* Feel free to use libraries or roll your own code as you see fit. However, please do not use a database or full text search product as we are interested to see how you write the solution.
* Where the data exists, values from any related entities should be included in the results, i.e. searching tickets should return their assigned user and searching users should return their assigned tickets.
* The user should be able to search on any field. Full value matching is fine (e.g. "mar" won't return "mary").
* The user should also be able to search for missing values, e.g. where a ticket does not have an assignee_id.
  Search can get complicated pretty easily, we just want to see that you can code a basic but efficient search application. Ideally, search response times should not increase linearly as the number of documents grows. You can assume all data can fit into memory on a single machine.

## How-to run

Launching the app should be as easy as
```shell
go run cmd/main.go
```
(keep in mind you would need to have `go` version `1.17` installed)

## Implementation details and considerations

1. Search is implemented with `Trie` data structure. It is a little more compact than HashMap but provides the same capabilities of full-match search and similar algorithmic complexity. Separate Trie is created for each search field although the alternative could be to use `$field_name+$field_value"` as a search key.

2. The internal representation of data is `map[string]interface{}` instead of `struct` for the sake of extensibility. Changing data format or adding new fields wouldn't require a lot of code change, only the change in the hardcoded meta-data in `data/data.go` file.

3. All fields are treated as strings, except arrays, which are treated as arrays of strings.

4. The combination of previous 2 factors enforced usage of reflection in 2 places: while indexing and while rendering the result.

5. Relations between data types are also hardcoded as a meta-data in `data/data.go`.

6. Tests exist and verify some general cases, although there is always space for improvement.

7. Libraries that are used for CLI rendering may have bugs and may not work perfectly. Didn't have a chance to work with any of them before.

8. Files with data are embedded into the binary and there is no way yet to pass it from outside.
