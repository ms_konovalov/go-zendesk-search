package search

import (
	"testing"

	"zendesk-search/model"

	"github.com/stretchr/testify/assert"
)

func TestPutGet(t *testing.T) {
	idx := CreateIndex()
	c := idx.getOrCreateCollection("test")
	c.put("name", "Ivan", model.Item{})
	c.put("name", "Dmitry", model.Item{})
	c.put("name", "Ivan", model.Item{})
	assert.Len(t, c.get("name", "Dmitry"), 1)
	assert.Len(t, c.get("name", "Ivan"), 2)
	assert.Empty(t, c.get("name", "Anton"))
	assert.Empty(t, c.get("age", "Ivan"))
}
