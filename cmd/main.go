package main

import (
	"os"

	"zendesk-search/cli"
)

func main() {
	os.Exit(cli.Do())
}
