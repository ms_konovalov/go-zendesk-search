package search

// Package search contains functions to add data to a search index and to search for data in a search index
import (
	"zendesk-search/data"
	"zendesk-search/model"
)

func (idx *Index) IndexData(data []model.DataType) error {
	for _, t := range data {
		collection := idx.getOrCreateCollection(t.Name)
		items, err := t.Load()
		if err != nil {
			return err
		}

		for _, item := range items {
			for _, field := range t.Fields {
				value := item[field]
				if value == nil {
					collection.put(field, model.ToString(value), item)
					continue
				}
				if model.IfArray(value) {
					values := value.([]interface{})
					for _, v := range values {
						collection.put(field, model.ToString(v), item)
					}
				} else {
					collection.put(field, model.ToString(value), item)
				}
			}

		}
	}
	return nil
}

func (idx *Index) SearchWithJoin(collection string, field string, key string) []model.Item {
	values := idx.Get(collection, field, key)
	if values == nil {
		return nil
	}
	for _, v := range values {
		for _, join := range data.Joins {
			idx.processJoinParty(collection, join.First, join.Second, v)
			idx.processJoinParty(collection, join.Second, join.First, v)
		}
	}
	return values
}

func (idx *Index) processJoinParty(collection string, primary model.JoinParty, secondary model.JoinParty, item model.Item) {
	if primary.Collection.Name == collection {
		joinKey, ok := item[primary.KeyField]
		if !ok {
			return
		}
		sel := idx.Get(secondary.Collection.Name, secondary.KeyField, model.ToString(joinKey))
		if sel == nil {
			return
		}
		values := mapValues(sel, primary.ForeignField)
		if len(values) == 1 {
			item[primary.Alias] = values[0]
		} else if len(values) > 1 {
			item[primary.Alias] = values
		}
	}
}

func mapValues(items []model.Item, field string) []model.Value {
	values := make([]model.Value, 0)
	for _, item := range items {
		v := item[field]
		if v == nil {
			continue
		}
		values = append(values, v)
	}
	return values
}
