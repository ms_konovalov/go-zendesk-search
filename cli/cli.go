package cli

import (
	"fmt"
	"os"
	"sort"

	"zendesk-search/data"
	"zendesk-search/model"
	"zendesk-search/search"

	"github.com/AlecAivazis/survey/v2"
	"github.com/olekukonko/tablewriter"
)

func Do() int {
	idx := search.CreateIndex()
	if err := idx.IndexData(data.SupportedTypes); err != nil {
		fmt.Printf("Error indexing data: %s", err.Error())
		return 1
	}

	fmt.Println("Welcome to Zendesk Search.")
	fmt.Println()

	answer := struct {
		Option string
	}{}

	qs := []*survey.Question{
		{
			Name: "option",
			Prompt: &survey.Select{
				Message: "Select search options:",
				Options: []string{"Search Zendesk", "View list of searchable fields", "Quit"},
			},
		},
	}

	for true {
		if err := survey.Ask(qs, &answer); err != nil {
			fmt.Println(err.Error())
			return 1
		}

		switch answer.Option {
		case "Quit":
			os.Exit(0)
		case "Search Zendesk":
			doSearch(idx)
		case "View list of searchable fields":
			printSearchFields()
		}
	}
	return 0
}

func printSearchFields() {
	fmt.Println()
	for _, t := range data.SupportedTypes {
		fmt.Println("---------------------")
		fmt.Println("Search", t.Name, "with")
		for _, f := range t.Fields {
			fmt.Println(f)
		}
	}
	fmt.Println("---------------------")
	fmt.Println()
	fmt.Println()
}

func doSearch(idx *search.Index) {
	answer := struct {
		Collection string
		Field      string
		Value      string
	}{}
	qs := []*survey.Question{
		{
			Name: "collection",
			Prompt: &survey.Select{
				Message: "Select collection:",
				Options: []string{data.UserType.Name, data.TicketType.Name, "Quit"},
			},
		},
	}
	if err := survey.Ask(qs, &answer); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	var fields []string
	if answer.Collection == "Quit" {
		os.Exit(0)
	}
	if answer.Collection == "Users" {
		fields = data.UserType.Fields
	} else {
		fields = data.TicketType.Fields
	}
	qs2 := []*survey.Question{
		{
			Name: "field",
			Prompt: &survey.Select{
				Message: "Select search term:",
				Options: fields,
			},
		},
		{
			Name:     "value",
			Prompt:   &survey.Input{Message: "Enter search value:"},
			Validate: survey.Required,
		},
	}
	if err := survey.Ask(qs2, &answer); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	items := idx.SearchWithJoin(answer.Collection, answer.Field, answer.Value)
	printItems(items)
	fmt.Println()
	fmt.Println()
}

func printItems(items []model.Item) {
	table := tablewriter.NewWriter(os.Stdout)
	fieldsSet := map[string]struct{}{}
	var fields []string
	for _, item := range items {
		for k := range item {
			_, ok := fieldsSet[k]
			if !ok {
				fieldsSet[k] = struct{}{}
				fields = append(fields, k)
			}
		}
	}
	sort.Strings(fields)
	table.SetHeader(fields)
	for _, item := range items {
		var values []string
		for _, f := range fields {
			values = append(values, model.ToString(item[f]))
		}
		table.Append(values)
	}
	table.SetColWidth(5)
	table.Render()
}
